#!/bin/bash

# This script is used to submit jobs from an XP directory.
#
# $1 - The executable of the experiment-ware.
# $2 - The output folder.
#      If this folder does not exist, it is created.
#      Otherwise, experiments that have not been run are re-submitted.

# Testing the parameters.
test $# -ne 2 && echo "Usage: $0 <xp-ware> <out-dir>" && exit 1

# Identifying the location of the campaign.
executable="$1"
# if [ -e "$campaign/.subxp" ]
# then
#     campaign="$campaign/.."
# fi

dir=$(dirname $0)


# Checking the configuration of the campaign.
test ! -f "$dir/../config/xp.properties" && echo "$0: xp.properties is missing." && exit 3
test ! -f "$dir/../input_set/jobs.lst" && echo "$0: jobs.lst is missing." && exit 4
test ! -x "$executable" && echo "$0: $executable is missing or not marked as executable." && exit 5

# Creating the output folder if it does not exist.
mkdir -p "$2"


# Resolving relative paths.
#set -- "$(readlink -e "$1")" "$(readlink -e "$2")"
echo "c submitted: $1"
echo "c output: $2"

# Getting the properties to apply.
while read -r property
do
    property=$(echo "$property" | sed 's/ *= */=/g')
    name=$(echo "$property" | cut -d '=' -f 1)
    value=$(echo "$property" | cut -d '=' -f 2)
    declare "$name"="$value"
done < "$dir/../config/xp.properties"

# Retrieving the CPU execution time.
cpuHours=$(echo "$cpuTimeLimit" | cut -d ':' -f 1)
cpuMinutes=$(echo "$cpuTimeLimit" | cut -d ':' -f 2)
cpuSeconds=$(echo "$cpuTimeLimit" | cut -d ':' -f 3)
totalCpuTime=$((cpuHours * 3600 + cpuMinutes * 60 + cpuSeconds))

# Retrieving the wall execution time.
wallHours=$(echo "$wallTimeLimit" | cut -d ':' -f 1)
wallMinutes=$(echo "$wallTimeLimit" | cut -d ':' -f 2)
wallSeconds=$(echo "$wallTimeLimit" | cut -d ':' -f 3)
totalWallTime=$((wallHours * 3600 + wallMinutes * 60 + wallSeconds))

# Retrieving the delay between the two signals SIGINT and SIGKILL.
delayHours=$(echo "$delay" | cut -d ':' -f 1)
delayMinutes=$(echo "$delay" | cut -d ':' -f 2)
delaySeconds=$(echo "$delay" | cut -d ':' -f 3)
totalDelay=$((delayHours * 3600 + delayMinutes * 60 + delaySeconds))

# Adding the delay for the Torque CPU time limit.
torqueCpuHours=$((cpuHours + delayHours))
torqueCpuMinutes=$((cpuMinutes + delayMinutes))
torqueCpuSeconds=$((cpuSeconds + delaySeconds))
torqueCpuTime="$torqueCpuHours:$torqueCpuMinutes:$torqueCpuSeconds"

# Adding the delay for the Torque wall time limit.
torqueWallHours=$((wallHours + delayHours))
torqueWallMinutes=$((wallMinutes + delayMinutes))
torqueWallSeconds=$((wallSeconds + delaySeconds))
torqueWallTime="$torqueWallHours:$torqueWallMinutes:$torqueWallSeconds"

# Adding the jobs.
while read -r job
do
    echo $(echo "$job" | sed 's/[-/: _=?~+]\+/_/g' | sed 's/^_*//g')

    # Computing the name of the output directory for this job.
    out="$2/$(echo "$job" | sed 's/[-/: _=?~+]\+/_/g' | sed 's/^_*//g')"

    # Checking whether the job must be submitted.
    if [ -e "$out/stdout" ] && [ -e "$out/stderr" ] && [ -e "$out/statistics.out" ] && [ -e "$out/execution.out" ]
    then
        # The job has already been run.
        echo -e "\e[96m"
        echo "c ignoring submission of '$job' (already executed)"
        echo -e "\e[39m"
        continue
    fi

    # Submitting the job (or at least, trying to).
    mkdir "$out"
    echo -e "\e[34m"
    echo "c qadd -e \"$out/stderr\" -o \"$out/stdout\" -q \"$queue\" -l cput=\"$torqueCpuTime\",walltime=\"$torqueWallTime\" -- $dir/launch \"$totalCpuTime\" \"$totalWallTime\" \"$memoryLimit\" \"$totalDelay\" \"$1\" \"\\\"$job\\\"\" \"$out\""
    echo -e "\e[39m"
    echo -e "\e[93m"
    qadd -e "$out/stderr" -o "$out/stdout" -q "$queue" -l cput="$torqueCpuTime",walltime="$torqueWallTime" -- $dir/launch "$totalCpuTime" "$totalWallTime" "$memoryLimit" "$totalDelay" "$1" "\"$job\"" "$out"
    echo -e "\e[39m"

    # Checking the submission status.
    added="$?"
    if [ "$added" -ne 0 ]
    then
        echo -e "\e[91m"
        echo "Failed to add job '$job' to queue '$queue': qadd exited with status $added."
        echo "Job submission has been aborted."
        echo "Please fix the issue (if any) before submitting again."
        echo -e "\e[39m"
        rmdir "$out"
        exit "$added"
    fi
done < "$dir/../input_set/jobs.lst"
