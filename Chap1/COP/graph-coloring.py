from pycsp3 import *

n_var, neighbors = data
regions = VarArray(size=n_var, dom=range(n_var))

satisfy(
    [regions[i] != regions[j] for i, j in neighbors]
)

minimize(
    Maximum(regions)
)