from pycsp3 import *

n_var, neighbors = data
regions = VarArray(size=n_var, dom=range(4))

satisfy(
    [regions[i] != regions[j] for i, j in neighbors]
)