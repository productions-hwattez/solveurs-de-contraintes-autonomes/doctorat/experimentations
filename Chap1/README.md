# Problème de coloration de carte appliqué à la carte de l'Inde

1. [Import de la carte de l'Inde et ses régions](import.ipynb)
2. Création des figures
    * [Création du tableau de contraintes .tex](constraint_maker.ipynb)
    * [Création de la carte de l'Inde vierge et exemple d'assignations incohérentes](empty_india.ipynb)
    * [Problème de coloration de cartes (version CSP) et génération d'une carte avec une solution et une autre avec incohérence](CSP/probleme_CSP.ipynb)
    * [Problème de coloration de cartes (version COP) et génération d'une carte avec une solution](COP/probleme_COP.ipynb)
