# Organisation de la campagne

## But de la campagne

Cette campagne a pour but de tester le tournoi d'heuristique avec la fonction de récompense *esb*.

## Description de la hiérarchie

- `config`
	- `metrics_scalpel.yml` : fichier de description de la campagne (logs des solveurs, tps cpu, mémoire...)
	- `xp.properties` : description propre au cluster de calcul utilisé
- `experiment_wares`
	- `./solveur1` : exécutable d'un solveur à exécuter sur l'ensemble des instances (contenues dans `input_set`)
	- `./solveur2` : exécutable d'un solveur à exécuter sur l'ensemble des instances (contenues dans `input_set`)
	- `./solveur3` : exécutable d'un solveur à exécuter sur l'ensemble des instances (contenues dans `input_set`)
	- `...` : ... autant que nécessaire pour compléter la campagne
- `input_set`
	- `...` : liste des instances ou un fichier listant l'emplacement des instances
- `experiments` : correspond à l'ensemble des logs classés hiérarchiquement par solveurs, puis par instances
    - `solveur1`
        - `instance1`
            - `execution.out` : la sortie standard du `solveur1` pour l'instance `instance1`
            - `statistics.out` : statistiques finales de temps et de mémoire construits par `runsolver`
            - `stdout` : la sortie standard de `runsolver` est redirigée dans ce fichier
            - `stderr` : la sortie d'erreur du solveur et de `runsolver` sont redirigées dans ce fichier
        - `instance2`
            - `...`
        - `instance3`
            - `...`
        - `...`
    - `solveur2`
        - `...`
    - `solveur3`
        - `...`
- `fig`
	- `[1-9][A-Z]-.*[svg|png|pdf|tex]` : 
        - l'ensemble des figures produites par les notebooks
        - chaque figure est indicée par la pattern suivant `[1-9][A-Z]-.*` mettant en lien la figure avec un notebook précis (la description des notebooks se fait plus bas)
- `tools`
	- `*.sh` : 
        - correspond à l'ensemble des scripts nécessaires à l'envoie de la présente campagne sur le cluster-B du CRIL-LENS
        - ces scripts nécessitent d'être adaptés pour fonctionner dans un autre environnement de calcul
- `export` : extraction et export des logs dans ce dossier
    - `[1-9]-*.[bin|csv]` :
        - les logs binaires sont volumineux et ne sont pas conservés dans le répertoire git
        - les logs csv sont allégés et conservés dans le répertoire git
        - les logs sont indexés par un nombre entre 1 et 9 en correspondances avec le notebook l'ayant produit
- `[1-9][A-Z]?.*ipynb` :
    - les titres des notebooks comportant l'extraction des logs des solveurs commencent par un chiffre (et sans lettre) et sont ordonnés par celui-ci. L'ordre doit être respecté car de précédents logs sont peut-être nécessaires.
    - les titres des notebooks comportant l'analyse des campagnes commencent par un nombre (correspondant à une extraction précise), puis une lettre. L'ordre n'est pas important. Chaque notebook produit une ou plusieurs figures.
    
    
## Informations complémentaires

- Lorsque le résultat des exports n'est pas trop conséquent en espace, ceux-ci sont commités sur le répertoire (en csv). Il est donc possible d'analyser directement ces données, sinon il est nécessaire d'exécuter à nouveau la campagne.
- Les outils mis à dispositions dans le répertoire *tools* sont adaptés pour le cluster du CRIL, ainsi que la disposition des logs dans le dossier *experiments* (ce dernier n'est pas visible dans le répertoire car il est trop encombrant). L'utilisateur souhaitant exécuter à nouveau une campagne devra adapter les outils et possiblement la disposition des logs et leur lecture grâce à l'outil *Scalpel* et son fichier de configuration `config/metrics_scalpel.yml`.