# Expérimentations de la thèse : *Solveurs de Contraintes Autonomes*

Pour les `Chap{i}`, où `i>2`, correspondent aux campagnes d'expérimentations entre solveur et instances :

- le solveur ACE utilisé lors des expérimentations comprenant l'ensemble des implantations de la thèse : [lien vers le répertoire](/../../../../ace)
- en ce qui concerne les instances de problèmes :
    - les COPs (`Chap7`) : [lien vers le répertoire](/../../../../instances-xcsp/-/tree/cop)
    - les CSPs (les autres `Chap{i}`) : [lien vers le répertoire](/../../../../instances-xcsp/-/tree/csp)
