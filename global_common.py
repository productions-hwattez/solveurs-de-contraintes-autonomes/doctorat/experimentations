import matplotlib as mpl
c = [x['color'] for x in mpl.rcParams["axes.prop_cycle"]]
from autograph.core.enumstyle import LineType
import os
FILE = __file__[:__file__.rfind('/')+1]

# LOGS EXPORTÉS

## Chapitre 3

ANALYSIS_01 = FILE + 'Chap3/01_ace/export/1-analysis.csv'

ANALYSIS_02 = FILE + 'Chap3/02_refining_constraint_weighting/export/1-analysis.csv'

ANALYSIS_03            = FILE + 'Chap3/03_select_h_and_tb/export/1-analysis.bin'
ANALYSIS_03_LIGHT      = FILE + 'Chap3/03_select_h_and_tb/export/2-analysis_light.csv'
ANALYSIS_03_LIGHT_BASE = FILE + 'Chap3/03_select_h_and_tb/export/3-analysis_light_base.csv'

## Chapitre 4

ANALYSIS_04       = FILE + 'Chap4/04_xiayap/export/1-analysis.bin'
ANALYSIS_04_LIGHT = FILE + 'Chap4/04_xiayap/export/2-analysis_light.csv'

ANALYSIS_05            = FILE + 'Chap4/05_restartframework/export/1-analysis.bin'
ANALYSIS_05_LIGHT      = FILE + 'Chap4/05_restartframework/export/2-analysis_light.csv'
ANALYSIS_05_MERGE      = FILE + 'Chap4/05_restartframework/export/3-analysis_merge.csv'
ANALYSIS_05_MERGE_FULL = FILE + 'Chap4/05_restartframework/export/3-analysis_merge_full.csv'

## Chapitre 5

ANALYSIS_10_AUVR                 = FILE + 'Chap5/10_auvr_cs_multivarh/export/1-analysis.bin'
ANALYSIS_10_AUVR_LIGHT           = FILE + 'Chap5/10_auvr_cs_multivarh/export/2-analysis_light.csv'
ANALYSIS_10_AUVR_MERGE           = FILE + 'Chap5/10_auvr_cs_multivarh/export/3-analysis_merge.csv'
ANALYSIS_10_ESB                  = FILE + 'Chap5/10_esb_cs_multivarh/export/1-analysis.bin'
ANALYSIS_10_ESB_LIGHT            = FILE + 'Chap5/10_esb_cs_multivarh/export/2-analysis_light.csv'
ANALYSIS_10_ESB_MERGE            = FILE + 'Chap5/10_esb_cs_multivarh/export/3-analysis_merge.csv'
ANALYSIS_10_NPTS                 = FILE + 'Chap5/10_npts_cs_multivarh/export/1-analysis.bin'
ANALYSIS_10_NPTS_LIGHT           = FILE + 'Chap5/10_npts_cs_multivarh/export/2-analysis_light.csv'
ANALYSIS_10_NPTS_MERGE           = FILE + 'Chap5/10_npts_cs_multivarh/export/3-analysis_merge.csv'
ANALYSIS_10_BEST_MERGE           = FILE + 'Chap5/10_best_cs_multivarh/export/1-analysis_merge.csv'
ANALYSIS_10_BEST_MERGE_FULL      = FILE + 'Chap5/10_best_cs_multivarh/export/1-analysis_merge_full.bin'
ANALYSIS_10_BEST_MERGE_PLUS_BASE = FILE + 'Chap5/10_best_cs_multivarh/export/1-analysis_merge_plus_base.csv'

## Chapitre 6

ANALYSIS_06       = FILE + 'Chap6/06_fixedrandperturb/export/1-analysis.csv'
ANALYSIS_06_MERGE = FILE + 'Chap6/06_fixedrandperturb/export/2-analysis_merge.csv'
ANALYSIS_06_BEST  = FILE + 'Chap6/06_fixedrandperturb_best/export/1-analysis.csv'

ANALYSIS_07       = FILE + 'Chap6/07_grimesgomes/export/1-analysis.csv'
ANALYSIS_07_MERGE = FILE + 'Chap6/07_grimesgomes/export/2-analysis_merge.csv'

ANALYSIS_08_AUVR       = FILE + 'Chap6/08_auvr_banditrandperturb/export/1-analysis.csv'
ANALYSIS_08_AUVR_MERGE = FILE + 'Chap6/08_auvr_banditrandperturb/export/2-analysis_merge.csv'
ANALYSIS_08_ESB        = FILE + 'Chap6/08_esb_banditrandperturb/export/1-analysis.csv'
ANALYSIS_08_ESB_MERGE  = FILE + 'Chap6/08_esb_banditrandperturb/export/2-analysis_merge.csv'
ANALYSIS_08_NPTS       = FILE + 'Chap6/08_npts_banditrandperturb/export/1-analysis.csv'
ANALYSIS_08_NPTS_MERGE = FILE + 'Chap6/08_npts_banditrandperturb/export/2-analysis_merge.csv'
ANALYSIS_08_BEST       = FILE + 'Chap6/08_banditrandperturb_best/export/1-analysis.bin'
ANALYSIS_08_BEST_LIGHT = FILE + 'Chap6/08_banditrandperturb_best/export/1-analysis.csv'

ANALYSIS_09_NPTS        = FILE + 'Chap6/09_npts_seedperturb/export/1-analysis.csv'
ANALYSIS_09_NPTS_MERGE  = FILE + 'Chap6/09_npts_seedperturb/export/2-analysis_merge.csv'
ANALYSIS_09_BEST        = FILE + 'Chap6/09_seedperturb_best/export/1-analysis.bin'
ANALYSIS_09_BEST_LIGHT  = FILE + 'Chap6/09_seedperturb_best/export/1-analysis.csv'
ANALYSIS_09_MERGE_LIGHT = FILE + 'Chap6/09_seedperturb_best/export/2-merge.csv'

## Chapitre 7

ANALYSIS_13     = FILE + 'Chap7/13_ace/export/1-analysis.bin'
ANALYSIS_13_AGG = FILE + 'Chap7/13_ace/export/2-analysis.csv'

ANALYSIS_14     = FILE + 'Chap7/14_cop_gap_allseq/export/1-analysis.bin'
ANALYSIS_14_AGG = FILE + 'Chap7/14_cop_gap_allseq/export/2-analysis.csv'

ANALYSIS_15     = FILE + 'Chap7/15_cop_gap_expprev_factors/export/1-analysis.bin'
ANALYSIS_15_AGG = FILE + 'Chap7/15_cop_gap_expprev_factors/export/2-analysis.csv'

ANALYSIS_16     = FILE + 'Chap7/16_select_h_and_tb/export/1-analysis.bin'
ANALYSIS_16_AGG = FILE + 'Chap7/16_select_h_and_tb/export/2-analysis.csv'

ANALYSIS_17     = FILE + 'Chap7/17_cs_multivarh/export/1-analysis.bin'
ANALYSIS_17_AGG = FILE + 'Chap7/17_cs_multivarh/export/2-analysis.csv'

ANALYSIS_18     = FILE + 'Chap7/18_cs_perturbvarh/export/1-analysis.bin'
ANALYSIS_18_AGG = FILE + 'Chap7/18_cs_perturbvarh/export/2-analysis.csv'

ORDER = [
    'activity',
    'impact',
    'domddeg',
    'chs',
    'cacd',
    'VBS'
]

ORDER_B = [
    'lex',
    'deg',
    'rand',
    'VBS'
]
