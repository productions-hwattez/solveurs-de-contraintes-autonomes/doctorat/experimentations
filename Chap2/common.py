import sys
sys.path.insert(0, '..')
from global_common import *
import random
import math
import numpy as np

class Bandit:

	def __init__(self, k):
		self._k = k
		self._t = 0

	def select(self):
		pass

	def maj(self, r, i):
		pass

class Uniform(Bandit):

	def __init__(self, **kwargs):
		super().__init__(**kwargs)

	def select(self):
		return random.randint(0, self._k - 1)

class EGreedy(Bandit):

	def __init__(self, epsilon, **kwargs):
		super().__init__(**kwargs)
		self._epsilon = epsilon
		self._n = [0] * self._k
		self._r = [0] * self._k

	def select(self):
		if self._t < self._k:
			return self._t
		if random.random() < self._epsilon:
			return random.randint(0, self._k - 1)
		
		return max(range(self._k), key=lambda i: self._r[i])

	def maj(self, r, i):
		self._t += 1
		self._n[i] += 1
		self._r[i] = (self._r[i] * (self._n[i] - 1) + r) / self._n[i]

class UCB(Bandit):

	def __init__(self, **kwargs):
		super().__init__(**kwargs)
		self._n = [0] * self._k
		self._r = [0] * self._k

	def _ucb_val(self, i):
		if self._n[i] == 0:
			return float('inf')
		return self._r[i] + math.sqrt(8 * math.log(self._t) / self._n[i])

	def select(self):
		return max(range(self._k), key=lambda i: self._ucb_val(i))

	def maj(self, r, i):
		self._t += 1
		self._n[i] += 1
		self._r[i] = (self._r[i] * (self._n[i] - 1) + r) / self._n[i]

class EXP3(Bandit):

	def __init__(self, **kwargs):
		super().__init__(**kwargs)
		self._pi = [1/self._k] * self._k
		self._rb = [0] * self._k

	def select(self):
		if self._t < self._k:
			return self._t
		return np.random.choice(self._k, p=self._pi)

	def maj(self, r, i):
		self._t+=1

		self._rb[i] += r / self._pi[i]
		
		if self._t > self._k:
			step_size = math.sqrt(math.log(self._k) / (self._t * self._k))
			z = sum((np.exp(step_size * rbi) for rbi in self._rb))
			self._pi = [np.exp(step_size * rbi) / z for rbi in self._rb]

